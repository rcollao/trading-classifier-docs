# Best way to save the model for JS is to use this

    $ tensorflowjs_converter --input_format=tf_saved_model --output_node_names='MobilenetV1/Predictions/Reshape_1' --saved_model_tags=serve ./a-buy-model-6/ ./model6-json

This will generate a TF model, which will be a GRAPH model, and while this is good because it can have a build in normalization layer (which I am using),
YOU WILL NOT BE ABLE TO TRAIN the model using JS if you do this.

## Exporting a Keras LAYER MODEL

If you export a keras layer model, it may be a bit slower than graph, and IT WILL NOT HAVE A NORMALIZATION LAYER.
To do so run this on your python model:

```python
    model.save('./path-to-save/model.h5', save_format='h5')
    # Then run this in the terminal to convert the keras h5 model to a layer model for tfjs
    ! tensorflowjs_converter --input_format=keras ./path-to-save/model.h5 ./path-to-save/output/
```

This will succesfully generate a KERAS LAYER MODEL, however if you use a Rescaling layer (for normalization) this will NOT work in JS.
You can manually open the resulting .json file in the output folder and look at the layers array.
Look for the Rescaling layer, and just delete that object.
In my case the model looks like this:

```json
{
  "class_name": "Rescaling",
  "config": {
    "name": "rescaling_5",
    "trainable": true,
    "batch_input_shape": [null, 100, 180, 4],
    "dtype": "float32",
    "scale": 0.00392156862745098,
    "offset": 0.0
  }
}
```

So just removing this object from the layers array is enough to solve the problem.
If you noticed scale is 0.003921.. which is just 1/255 as that was the normalization being used by the layer in python:

```python
    layers.Rescaling(1./255, input_shape=(img_height, img_width, 4)),
```
