const moment = require("moment");
const data = require("./data.json");
const fs = require("fs");

const result = [];
data.map((d) => {
  const start = moment(d[2]);
  const end = moment(d[3]);
  const period = end - start;
  const newStart = moment(start - period);
  result.push([
    d[0],
    d[1],
    newStart.format("YYYY-MM-DD HH:mm"),
    start.format("YYYY-MM-DD HH:mm"),
  ]);
});

fs.writeFileSync("./output.json", JSON.stringify(result, null, 2));
