const tf = require("@tensorflow/tfjs");
const tfnode = require("@tensorflow/tfjs-node");
const fs = require("fs");
const numberOfClases = 2;
const imageHeight = 100;
const imageWidth = 180;

async function main() {
  const model = tf.sequential({
    layers: [
      tf.layers.inputLayer({
        batchInputShape: [null, imageHeight, imageWidth, 4],
        sparse: false,
        name: "input-layer",
      }),
      tf.layers.conv2d({
        name: "conv2d-1",
        filters: 64,
        kernelSize: 3,
        padding: "same",
        activation: "relu",
      }),
      tf.layers.maxPool2d({ name: "first-max-pool" }),
      tf.layers.conv2d({
        filters: 128,
        kernelSize: 3,
        padding: "same",
        activation: "relu",
      }),
      tf.layers.maxPool2d({}),
      tf.layers.conv2d({
        filters: 256,
        kernelSize: 3,
        padding: "same",
        activation: "relu",
      }),
      tf.layers.maxPool2d({}),
      tf.layers.flatten(),
      tf.layers.dense({ units: 256, activation: "relu" }),
      tf.layers.dense({ units: numberOfClases, name: "output-label" }),
    ],
  });
  model.compile({
    optimizer: "adam",
    loss: tf.losses.sigmoidCrossEntropy,
    metrics: ["accuracy"],
  });

  //   model.summary();
  //   model.save(handlerLayer);
  //   return;
  //build data for training
  const buyModelPath = "./python-notebooks/output/fromMatt2/buyModel";
  const otherPath = "./python-notebooks/output/fromMatt2/other";
  const buyModelPics = fs.readdirSync(buyModelPath);
  const otherPics = fs.readdirSync(otherPath);
  const buyLabels = []; //tf.tensor2d([], [buyModelPics.length, 2]);
  const otherLabels = []; // tf.tensor2d([], [otherPics.length, 2]);
  let buyModelTensors = buyModelPics.map((picPath) => {
    buyLabels.push([1, 0]);
    // buyLabels.push({ "output-label": tf.tensor2d([[1, 0]]) });
    // buyLabels.push({ "output-label": [1, 0] });
    // buyLabels = buyLabels.concat([1, 0]);
    const img = imageToTensor(buyModelPath + "/" + picPath, true);
    return img;
  });
  let otherTensors = otherPics.map((picPath) => {
    otherLabels.push([0, 1]);
    // buyLabels.push({'output-label':1});
    // otherLabels.push({ "output-label": tf.tensor2d([[0, 1]]) });
    // otherLabels = otherLabels.concat([0, 1]);
    return imageToTensor(otherPath + "/" + picPath, true);
  });
  //   const buyLabelsTensor = tf.tensor2d(buyLabels);
  //   const otherLabelsTensor = tf.tensor2d(otherLabels);

  //   const xDataset = tf.data.array(buyModelTensors);
  //   const res = await xDataset.toArray();
  //   console.log(res);
  //   const yDataset = tf.data.array(buyLabels);
  //   const xyDataset = tf.data.zip({ xs: xDataset, ys: yDataset }).batch(5);
  //   // .shuffle(4);
  //   model.batchInputShape = [null, imageHeight, imageWidth, 4];

  //   const res2 = await xyDataset.toArray();
  //   console.log(res2);

  //   model.trainOnBatch(xDataset,yDataset)
  //   const ds = tf.data.generator(()=>test).batch(10);

  //   const resFir = await model.fitDataset(xyDataset, {
  //     epochs: 10,
  //     batchesPerEpoch: 5,
  //     //   batchSize:4,
  //     //   batch:4,
  //     // batchesPerEpoch: 32,
  //     callbacks: { onEpochEnd: (epoch, logs) => console.log(logs.loss) },
  //   });
  //   console.log("Hello..", resFir.history.acc);
  // ! I did not know about tf.concat until I saw:
  //! https://stackoverflow.com/a/54703354
  const x = tf.concat([...buyModelTensors, ...otherTensors], 0);
  const y = tf.tensor2d([...buyLabels, ...otherLabels]);
  const resFit = await model.fit(x, y, {
    batchSize: 32,
    epochs: 2,
  });
  console.log("resFit", resFit);
  //TODO uncomment this if you want to save the model
  //   const saveHandler = tfnode.io.fileSystem("./temp/");
  //   model.save(saveHandler);
}

function imageToTensor(path, normalize = false) {
  const picBuff = fs.readFileSync(path);
  // const picBuff = fs.readFileSync("./afail4.png");
  //Decode image with tfnode into the 4 channels of RGBA
  //! its is important to convert toFloat()
  const inputPic = tfnode.node.decodePng(picBuff, 4).toFloat(); //4 is for RGBA number of channels

  //!Resize image using nearestNeighbor algo
  //   const resized = tf.image.resizeBilinear(inputPic, [100, 180]);
  const resized = tf.image.resizeNearestNeighbor(inputPic, [100, 180]);

  let normalized = resized;
  //Just a scalar tensor value to make operations
  if (normalize) {
    const offset = tf.scalar(255);
    //This is how you would normalized dividing each element by offset
    normalized = resized.div(offset);
  }

  //! this step is important I dont know why :sweat_smile:
  const final = normalized.expandDims(0);
  return final;
}

main()
  .then(() => {
    console.log("Exit");
  })
  .catch((e) => {
    console.error(e);
  });
