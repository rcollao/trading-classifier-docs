// import * as tf from '@tensorflow/tfjs-node'
const tf = require("@tensorflow/tfjs");
const tfnode = require("@tensorflow/tfjs-node");
const fs = require("fs");
// tf.GraphModel
/**
 * @typedef {import("@tensorflow/tfjs-converter/dist/executor/graph_model")} GraphModel
 */
/** @type {GraphModel}*/
let model;
async function main() {
  //!Loading a GraphModel
  //----------------- Graph Model -------------------------
  //   const shouldNormalize = false;
  //   const handler = tfnode.io.fileSystem(
  //     "C:/Users/Rodrigo/AlgoTrading/model6-json/model.json"
  //   );
  // // const handler = tfnode.io.fileSystem("./test-model-graph/model.json");
  //   const myModel = await tf.loadGraphModel(handler);
  //----------------- Graph Model -------------------------

  //!Loading a Layer Model
  //----------------- LAYER MODEL ------------------------
  const shouldNormalize = true;
  const handlerLayer = tfnode.io.fileSystem(
    "C:/Users/Rodrigo/AlgoTrading/a-buy-model-6-keras/test/model.json"
  );
  // const handlerLayer = tfnode.io.fileSystem("./test-model-layer/model.json");
  // const handlerLayer = tfnode.io.fileSystem("./temp/model.json");

  const myModel = await tf.loadLayersModel(handlerLayer);

  myModel.compile({
    optimizer: "adam",
    loss: tf.losses.sigmoidCrossEntropy,
    metrics: ["accuracy"],
  });
  //----------------- LAYER MODEL ------------------------

  model = myModel;
  //! If you are using Graph Model, then you do NOT need to normalize, the model will normalize itself
  console.time("predict");
  let res = preditForPicturePath("./120.png", shouldNormalize);
  // let res = preditForPicturePath("./afail4.png", shouldNormalize);
  console.log(res);
  console.timeEnd("predict");

  console.log("Training model with the same picture");
  await fitModel("120.png", "other", 2, shouldNormalize);

  console.log(
    "--------------Result after training (should be 100% accuracy)--------------"
  );
  let secondPrediction = preditForPicturePath("./120.png", shouldNormalize);
  console.log(secondPrediction);

  //   saveModel("./temp");
}

/**
 *
 * @param {string} path picture path
 * @param {'buyModel'|'other'} label label for the picture you are training
 * @param {number} epochs epoch value
 */
async function fitModel(path, label, epochs = 2, shouldNormalize = false) {
  let labelTf;
  //label should be like a table, where column one is buyModel and column two is other, and each row is a picture classification
  if (label == "buyModel") {
    labelTf = tf.tensor2d([[1, 0]]);
  } else if (label == "other") {
    labelTf = tf.tensor2d([[0, 1]]);
  }

  const imageTensor = imageToTensor(path,shouldNormalize);
  function onBatchEnd(batch, logs) {
    console.log("Accuracy", logs.acc);
  }
  const resFit = await model.fit(imageTensor, labelTf, {
    epochs: epochs,
    batchSize: 1,
    callbacks: { onBatchEnd },
  });
  // .then((info) => {
  //   console.log("Final accuracy", info.history.acc);
  // });
  console.log("Final Accuracy", resFit.history.acc);
}

function imageToTensor(path, normalize = false) {
  const picBuff = fs.readFileSync(path);
  // const picBuff = fs.readFileSync("./afail4.png");
  //Decode image with tfnode into the 4 channels of RGBA
  //! its is important to convert toFloat()
  const inputPic = tfnode.node.decodePng(picBuff, 4).toFloat(); //4 is for RGBA number of channels

  //!Resize image using nearestNeighbor algo
  //   const resized = tf.image.resizeBilinear(inputPic, [100, 180]);
  const resized = tf.image.resizeNearestNeighbor(inputPic, [100, 180]);

  let normalized = resized;
  //Just a scalar tensor value to make operations
  if (normalize) {
    const offset = tf.scalar(255);
    //This is how you would normalized dividing each element by offset
    normalized = resized.div(offset);
  }

  //! this step is important I dont know why :sweat_smile:
  const final = normalized.expandDims(0);
  return final;
}

function preditForPicturePath(path, normalize) {
  const final = imageToTensor(path, normalize);

  const prediction = model.predict(final);
  //   prediction.print();

  const label = prediction.argMax((axis = 1));
  //   label.print(); //! I think this is the label where 0 is buyModel and 1 is other

  //   tf.softmax(prediction).print(); //! softmax gives you the % of certaintity on the prediction

  const res = tf.softmax(prediction).dataSync();

  const labelValue = label.dataSync()[0];

  //   console.log(
  //     `${labelValue == 0 ? "Buy Model" : "Other"}, with a ${(
  //       res[labelValue] * 100
  //     ).toFixed(2)} accuracy`
  //   );
  return {
    model: labelValue == 0 ? "Buy Model" : "Other",
    accuracy: (res[labelValue] * 100).toFixed(2),
  };
}

function saveModel(name) {
  const handlerOutputModel = tfnode.io.fileSystem(name);
  model.save(handlerOutputModel).then((t) => {
    console.log(t);
  });
}

main()
  .then(() => {
    console.log("Exit");
  })
  .catch((e) => {
    console.log(e);
  });
